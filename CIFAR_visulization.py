# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 15:28:21 2016

@author: cat
"""
import numpy as np
import matplotlib.pyplot as plt

# plot results of different cells
data1= np.load('Mom_1e-4_reg.npz')
data2 = np.load('Mom_1e-3_reg.npz')
data3 = np.load('Mom_1e-2_reg.npz')

stepCount = data1['stepCount']
acc_train1 = data1['trainAccHistory']
acc_test1 = data1['testAccHistory']
loss1 = data1['lossHistory']
acc_train2 = data2['trainAccHistory']
acc_test2 = data2['testAccHistory']
loss2 = data2['lossHistory']
acc_train3 = data3['trainAccHistory']
acc_test3 = data3['testAccHistory']
loss3 = data3['lossHistory']

fig1 = plt.figure()
line1, = plt.plot(stepCount, acc_train1,color = 'b')
line2, = plt.plot(stepCount, acc_test1, color = 'b', linestyle = '--')
line3, = plt.plot(stepCount, acc_train2,color = 'g')
line4, = plt.plot(stepCount, acc_test2, color = 'g', linestyle = '--')
line5, = plt.plot(stepCount, acc_train3,color = 'r')
line6, = plt.plot(stepCount, acc_test3, color = 'r', linestyle = '--')

plt.legend([line1, line2, line3, line4, line5, line6], ["Train accuracy_learning rate=1e-4", 'Testing accuracy_learning rate=1e-4',
           "Train accuracy_learning rate=1e-3", "Testing accuracy_learning rate=1e-3",
           "Train accuracy_learning rate=1e-2", "Testing accuracy_learning rate=1e-2"], 
           loc = 'upper right')
plt.xlabel('Number of iterations')
plt.ylabel('Accuracy')
plt.title('Training and testing accuracy against number of iterations')
plt.show()

fig2 = plt.figure()
line1, = plt.plot(stepCount, loss1,color = 'b')
line2, = plt.plot(stepCount, loss2, color = 'g')
line3, = plt.plot(stepCount, loss3, color = 'r')
plt.legend([line1, line2, line3], ["Train loss_learning rate=1e-4", "Train loss_learning rate=1e-3",
           "Train loss_learning rate=1e-2"], 
           loc = 'lower left')
plt.xlabel('Number of iterations')
plt.ylabel('Train loss')
plt.title('Training loss against number of iterations')
plt.show()







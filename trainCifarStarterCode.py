from scipy import misc
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import os
# --------------------------------------------------
# setup
def weight_variable(shape, name):
    '''
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    '''
    # IMPLEMENT YOUR WEIGHT_VARIABLE HERE
    #W = tf.truncated_normal(shape, stddev=0.1, seed = None, dtype = tf.float32)  
    W = tf.get_variable(name, shape = shape, 
                        initializer=tf.contrib.layers.xavier_initializer_conv2d())
  
    return W
    
def bias_variable(shape):
    '''
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    '''
    # IMPLEMENT YOUR BIAS_VARIABLE HERE  
    b = tf.Variable(tf.constant(0.1, shape=shape) )
    return b
    
def conv2d(x, W):
    '''
    Perform 2-D convolution
    :param x: input tensor of size [N, W, H, Cin] where
    N: the number of images
    W: width of images
    H: height of images
    Cin: the number of channels of images
    :param W: weight tensor [w, h, Cin, Cout]
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters = the number of channels of images
    Cout: the number of filters
    :return: a tensor of features extracted by the filters, a.k.a. the results after convolution
    '''
    # IMPLEMENT YOUR CONV2D HERE
    h_conv = tf.nn.conv2d(x, W, strides = [1, 1, 1, 1], padding = 'SAME')
    return h_conv
def relu(x):
    h_relu = tf.nn.relu(x)
    return h_relu
    
def max_pool_2x2(x):
    '''
    Perform non-overlapping 2-D maxpooling on 2x2 regions in the input data
    :param x: input data
    :return: the results of maxpooling (max-marginalized + downsampling)
    '''
    # IMPLEMENT YOUR MAX_POOL_2X2 HERE
    h_max = tf.nn.max_pool(x, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding = 'SAME')
    return h_max

def variable_summaries(var, name):
    """Attach min, max, mean, std and histogram summaries to a Tensor"""
    with tf.name_scope('summaries'):
        tf.scalar_summary('min/' + name, tf.reduce_min(var))
        tf.scalar_summary('max/' + name, tf.reduce_max(var))
        mean = tf.reduce_mean(var)
        tf.scalar_summary('mean/' + name, mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.scalar_summary('stddev/' + name, stddev)
        tf.histogram_summary(name, var)   
    
def visualize_weights(kernel, grid_Y, grid_X, pad = 1):
    '''Visualize conv. features as an image (mostly for the 1st layer).
    Place kernel into a grid, with some paddings between adjacent filters.

    Args:
      kernel:            tensor of shape [Y, X, NumChannels, NumKernels]
      (grid_Y, grid_X):  shape of the grid. Require: NumKernels == grid_Y * grid_X
                           User is responsible of how to break into two multiples.
      pad:               number of black pixels around each filter (between them)

    Return:
      Tensor of shape [(Y+2*pad)*grid_Y, (X+2*pad)*grid_X, NumChannels, 1].
    '''
    x_min = tf.reduce_min(kernel)
    x_max = tf.reduce_max(kernel)

    kernel1 = (kernel - x_min) / (x_max - x_min)

    # pad X and Y
    x1 = tf.pad(kernel1, tf.constant( [[pad,pad],[pad, pad],[0,0],[0,0]] ), mode = 'CONSTANT')

    # X and Y dimensions, w.r.t. padding
    Y = kernel1.get_shape()[0] + 2 * pad
    X = kernel1.get_shape()[1] + 2 * pad

    channels = kernel1.get_shape()[2]

    # put NumKernels to the 1st dimension
    x2 = tf.transpose(x1, (3, 0, 1, 2))
    # organize grid on Y axis
    x3 = tf.reshape(x2, tf.pack([grid_X, Y * grid_Y, X, channels])) #3

    # switch X and Y axes
    x4 = tf.transpose(x3, (0, 2, 1, 3))
    # organize grid on X axis
    x5 = tf.reshape(x4, tf.pack([1, X * grid_X, Y * grid_Y, channels])) #3

    # back to normal order (not combining with the next step for clarity)
    x6 = tf.transpose(x5, (2, 1, 3, 0))

    # to tf.image_summary order [batch_size, height, width, channels],
    #   where in this case batch_size == 1
    x7 = tf.transpose(x6, (3, 0, 1, 2))

    # scale to [0, 255] and convert to uint8
    return tf.image.convert_image_dtype(x7, dtype = tf.uint8) 
    
    
ntrain = 1000 # per class
ntest = 100 # per class
nclass = 10 # number of classes
imsize = 28
nchannels = 1
batchsize = 32
Train = np.zeros((ntrain*nclass,imsize,imsize,nchannels))
Test = np.zeros((ntest*nclass,imsize,imsize,nchannels))
LTrain = np.zeros((ntrain*nclass,nclass))
LTest = np.zeros((ntest*nclass,nclass))
itrain = -1
itest = -1
result_dir = './results'

for iclass in range(0, nclass):
    for isample in range(0, ntrain):
        path = './CIFAR10/Train/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itrain += 1
        Train[itrain,:,:,0] = im
        LTrain[itrain,iclass] = 1 # 1-hot lable
    for isample in range(0, ntest):
        path = './CIFAR10/Test/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itest += 1
        Test[itest,:,:,0] = im
        LTest[itest,iclass] = 1 # 1-hot label
        
sess = tf.InteractiveSession()
tf_data = tf.placeholder(tf.float32, shape=(None, imsize, imsize, nchannels))#tf variable for the data, remember shape is [None, width, height, numberOfChannels] 
tf_labels = tf.placeholder(tf.float32, shape = (None, nclass))#tf variable for labels
keep_prob = tf.placeholder(tf.float32)
# --------------------------------------------------
# model
# create your model
conv1_weights = weight_variable(shape = [5, 5, nchannels, 32],name = "conv1")
conv1_bias = bias_variable(shape=[32])
conv1 = conv2d(tf_data, conv1_weights)
relu1 = tf.nn.relu(tf.nn.bias_add(conv1, conv1_bias))
pool1 = max_pool_2x2(relu1)
with tf.name_scope('conv1'):
        with tf.name_scope('conv1_activations_after_ReLU'):
            variable_summaries(relu1,'conv1_activations_after_ReLU')
        with tf.name_scope('conv1_activations_after_Pool'):
            variable_summaries(pool1,'conv1_activations_after_Pool')
            
conv2_weights = weight_variable(shape = [5, 5, 32, 64],name = "conv2")
conv2_bias = bias_variable(shape = [64])
conv2 = conv2d(pool1, conv2_weights)
relu2 = tf.nn.relu(tf.nn.bias_add(conv2, conv2_bias))
pool2 = max_pool_2x2(relu2)
with tf.name_scope('conv2'):
        with tf.name_scope('conv2_activations_after_ReLU'):
            variable_summaries(relu2,'conv2_activations_after_ReLU')
        with tf.name_scope('conv2_activations_after_Pool'):
            variable_summaries(pool2,'conv2_activations_after_Pool')
    
fc1_weights = weight_variable(shape = [(imsize/4)*(imsize/4)*64, 1024],name = "fc1")
fc1_bias = bias_variable(shape = [1024]) 
pool_shape = pool2.get_shape().as_list()
pool2_flat = tf.reshape(pool2, [-1, pool_shape[1]*pool_shape[2]*pool_shape[3]])
fc1 = tf.nn.relu(tf.matmul(pool2_flat, fc1_weights) + fc1_bias)
fc1_dropout = tf.nn.dropout(fc1, keep_prob)


fc2_weights = weight_variable(shape = [1024, nclass],name = "fc2")
fc2_bias = bias_variable(shape = [nclass])
logits = tf.matmul(fc1_dropout, fc2_weights) + fc2_bias   

grid = visualize_weights(conv1_weights, 8, 4)
filter_summary = tf.image_summary("filters", grid)
# --------------------------------------------------------
# loss
# set up the loss, optimization, evaluation, and accuracy
# compute cross-entropy loss
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits, tf.to_float(tf_labels)))
#loss = tf.reduce_mean(-tf.reduce_sum(tf_labels * tf.log(y_pred), reduction_indices=[1]))
y_pred = tf.nn.softmax(logits)
correct_pred = tf.equal(tf.argmax(y_pred, 1), tf.argmax(tf_labels, 1)) 
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

### L2 regularization
regularizers = (tf.nn.l2_loss(fc1_weights) + tf.nn.l2_loss(fc1_bias) +
                  tf.nn.l2_loss(fc2_weights) + tf.nn.l2_loss(fc2_bias))
loss += 5e-4 * regularizers

# optimization
# momentum optimizer
batch = tf.Variable(0, dtype = tf.float32)
learning_rate = tf.train.exponential_decay(
      1e-4,                # Base learning rate.
      batch * batchsize,  # Current index into the dataset.
      ntrain * nclass,          # Decay step.
      0.95,                # Decay rate.
      staircase=True)
#optimizer = tf.train.MomentumOptimizer(learning_rate, 0.95).minimize(loss, global_step = batch)
# adam optimizer
optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss)

summary_op = tf.merge_all_summaries()
saver = tf.train.Saver()
summary_writer = tf.train.SummaryWriter(result_dir, sess.graph)

sess.run(tf.initialize_all_variables())
stepCount = []
lossHistory = []
trainAccHistory = []
testAccHistory = []
batch_xs = np.zeros((batchsize, imsize, imsize, nchannels))#setup as [batchsize, width, height, numberOfChannels] and use np.zeros()
batch_ys = np.zeros((batchsize, nclass))#setup as [batchsize, the how many classes] 
maxStep = 10000
for i in range(maxStep): # try a small iteration size once it works then continue
    perm = np.arange(ntrain*nclass)
    np.random.shuffle(perm)
    for j in range(batchsize):
        batch_xs[j,:,:,:] = Train[perm[j],:,:,:]
        batch_ys[j,:] = LTrain[perm[j],:]
    if i%100 == 0:
        #calculate train accuracy and print it
        train_loss = loss.eval(feed_dict = {tf_data: batch_xs,
                   tf_labels: batch_ys,
                   keep_prob: 1.0} )
        train_accuracy = accuracy.eval(feed_dict = {tf_data: batch_xs,
                   tf_labels: batch_ys,
                   keep_prob: 1.0} )
        test_accuracy = accuracy.eval(feed_dict = {tf_data: Test,
                   tf_labels: LTest,
                   keep_prob: 1.0} )
        stepCount.append(i)
        lossHistory.append(train_loss)
        trainAccHistory.append(train_accuracy)
        testAccHistory.append(test_accuracy)
        print("--train loss at step %d is %g"%(i, train_loss))
        print("train accuracy at step %d is %g"%(i, train_accuracy))
        print("test accuracy at step %d is %g"%(i, test_accuracy))
        
        summary_str = sess.run(summary_op, feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0})
        summary_writer.add_summary(summary_str, i)
        summary_writer.flush()
    if i%2000 == 0 or i == maxStep-1:    
        checkpoint_file = os.path.join(result_dir, 'checkpoint')
        saver.save(sess, checkpoint_file, global_step=i)
    
            
    optimizer.run(feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 0.5}) # dropout only during training


# --------------------------------------------------
# test
print("test accuracy %g"%accuracy.eval(feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0}))
sess.close()

#np.savez("summary", stepCount = stepCount, lossHistory = lossHistory, 
#         trainAccHistory = trainAccHistory, testAccHistory = testAccHistory)
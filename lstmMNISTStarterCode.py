import tensorflow as tf 
from tensorflow.python.ops import rnn, rnn_cell
import numpy as np 
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt

mnist = input_data.read_data_sets("/tmp/data", one_hot = True)#call mnist function
learningRate = 0.001
trainingIters = 100000
batchSize = 128
displayStep = 10
nInput = 28#we want the input to take the 28 pixels
nSteps = 28#every 28
nHidden = 128#number of neurons for the RNN
nClasses = 10#this is MNIST so you know
x = tf.placeholder('float', [None, nSteps, nInput])
y = tf.placeholder('float', [None, nClasses])
weights = {
    'out': tf.Variable(tf.random_normal([nHidden, nClasses]))
}
biases = {
    'out': tf.Variable(tf.random_normal([nClasses]))
}
def RNN(x, weights, biases):
    x = tf.transpose(x, [1,0,2])
    x = tf.reshape(x, [-1, nInput])
    x = tf.split(0, nSteps, x) #configuring so you can get it as needed for the 28 pixels
#    rnnCell = rnn_cell.BasicRNNCell(nHidden)
#    outputs, states = rnn.rnn(rnnCell, x, dtype = tf.float32)#for the rnn where to get the output and hidden state   
    
    lstmCell = rnn_cell.BasicLSTMCell(nHidden, forget_bias = 1.0)#find which lstm to use in the documentation       
    outputs, states = rnn.rnn(lstmCell, x, dtype = tf.float32)#for the rnn where to get the output and hidden state 

#    GRUCell = rnn_cell.GRUCell(nHidden)   
#    outputs, states = rnn.rnn(GRUCell, x, dtype = tf.float32)#for the rnn where to get the output and hidden state 
#   
    return tf.matmul(outputs[-1], weights['out'])+ biases['out']
pred = RNN(x, weights, biases)

#optimization
#create the cost, optimization, evaluation, and accuracy
#for the cost softmax_cross_entropy_with_logits seems really good
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
optimizer = tf.train.AdamOptimizer(learning_rate = learningRate).minimize(cost)
correctPred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correctPred, tf.float32))
init = tf.initialize_all_variables()
with tf.Session() as sess:
    sess.run(init)
    step = 1
    stepCount = []
    lossHistory = []
    trainAccHistory = []
    testAccHistory = []
    testData = mnist.test.images.reshape((-1, nSteps, nInput))
    testLabel = mnist.test.labels     
    while step* batchSize < trainingIters:
        batchX, batchY = mnist.train.next_batch(batchSize) #mnist has a way to get the next batch
        batchX = batchX.reshape((batchSize, nSteps, nInput))
        sess.run(optimizer, feed_dict={x: batchX, y: batchY})
        if step % displayStep == 0:
            stepCount.append(step)
            acc = sess.run(accuracy, feed_dict = {x:batchX, y:batchY})
            trainAccHistory.append(acc)
            loss = sess.run(cost, feed_dict = {x:batchX, y:batchY})
            lossHistory.append(loss)
            print("Iter " + str(step*batchSize) + ", Minibatch Loss= " + \
                  "{:.6f}".format(loss) + ", Training Accuracy= " + \
                  "{:.5f}".format(acc))         
            testAcc = sess.run(accuracy, feed_dict={x:testData, y:testLabel})
            testAccHistory.append(testAcc)            
            print("Testing Accuracy: " + "{:.5f}".format(testAcc))
        step += 1
    print('Optimization finished')
    
#plot the results
fig1 = plt.figure()
plt.plot(np.array(stepCount)*128, lossHistory)
plt.xlabel('Number of iterations')
plt.ylabel('Training loss')
plt.title('Training loss against number of training iterations')
plt.show()

fig2 = plt.figure()
line1, = plt.plot(np.array(stepCount)*128, trainAccHistory,'b')
line2, = plt.plot(np.array(stepCount)*128, testAccHistory, 'g')
plt.legend([line1, line2], ["Train accuracy", 'Testing accuracy'], loc = 'lower right')
plt.xlabel('Number of iterations')
plt.ylabel('Accuracy')
plt.title('Training and testing accuracy against number of training iterations')
plt.show()

# save data
np.savez("lstmfile128", stepCount = stepCount, lossHistory = lossHistory, 
         trainAccHistory = trainAccHistory, testAccHistory = testAccHistory)

# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 15:28:21 2016

@author: cat
"""
import numpy as np
import matplotlib.pyplot as plt
from tempfile import TemporaryFile

# plot results of different cells
rnn = np.load('rnnfile.npz')
lstm = np.load('lstmfile.npz')
gru = np.load('grufile.npz')

stepCount = rnn['stepCount']
rnn_train = rnn['trainAccHistory']
rnn_test = rnn['testAccHistory']
lstm_train = lstm['trainAccHistory']
lstm_test = lstm['testAccHistory']
gru_train = gru['trainAccHistory']
gru_test = gru['testAccHistory']

fig1 = plt.figure()
line1, = plt.plot(stepCount*128, rnn_train,color = 'b')
line2, = plt.plot(stepCount*128, rnn_test, color = 'b', linestyle = '--')
line3, = plt.plot(stepCount*128, lstm_train,color = 'g')
line4, = plt.plot(stepCount*128, lstm_test, color = 'g', linestyle = '--')
line5, = plt.plot(stepCount*128, gru_train,color = 'r')
line6, = plt.plot(stepCount*128, gru_test, color = 'r', linestyle = '--')
plt.legend([line1, line2, line3, line4, line5, line6], ["Train accuracy_RNN", 'Testing accuracy_RNN', 
           "Train accuracy_LSTM", 'Testing accuracy_LSTM', "Train accuracy_GRU", 'Testing accuracy_GRU'], 
           loc = 'lower right')
plt.xlabel('Number of iterations')
plt.ylabel('Accuracy')
plt.title('Training and testing accuracy against number of iterations')
plt.show()

# plot accuracy of different hidden units for LSTM
fig2 = plt.figure()
lstm_32 = np.load('lstmfile32.npz')
lstm_64 = np.load('lstmfile64.npz')
lstm_128 = np.load('lstmfile.npz')
lstm_256 = np.load('lstmfile256.npz')
stepCount = lstm['stepCount']
lstm_32_train = lstm_32['trainAccHistory']
lstm_32_test = lstm_32['testAccHistory']
lstm_64_train = lstm_64['trainAccHistory']
lstm_64_test = lstm_64['testAccHistory']
lstm_128_train = lstm_128['trainAccHistory']
lstm_128_test = lstm_128['testAccHistory']
lstm_256_train = lstm_256['trainAccHistory']
lstm_256_test = lstm_256['testAccHistory']

line1, = plt.plot(stepCount*128, lstm_32_train,color = 'b')
line2, = plt.plot(stepCount*128, lstm_32_test, color = 'b', linestyle = '--')
line3, = plt.plot(stepCount*128, lstm_64_train,color = 'g')
line4, = plt.plot(stepCount*128, lstm_64_test, color = 'g', linestyle = '--')
line5, = plt.plot(stepCount*128, lstm_128_train,color = 'y')
line6, = plt.plot(stepCount*128, lstm_128_test, color = 'y', linestyle = '--')
line7, = plt.plot(stepCount*128, lstm_256_train,color = 'r')
line8, = plt.plot(stepCount*128, lstm_256_test, color = 'r', linestyle = '--')
plt.legend([line1, line2, line3, line4, line5, line6], ["Train accuracy_32 units", 'Testing accuracy_32 units', 
           "Train accuracy_64 units", 'Testing accuracy_64 units', "Train accuracy_128 units", 'Testing accuracy_128 units',
           "Train accuracy_256 units", 'Testing accuracy_256 units'], 
           loc = 'lower right')
plt.xlabel('Number of iterations')
plt.ylabel('Accuracy')
plt.title('Training and testing accuracy against number of iterations')
plt.show()

# plot train loss of different hidden units for LSTM
fig3 = plt.figure()
lstm_32_loss = lstm_32['lossHistory']
lstm_64_loss = lstm_64['lossHistory']
lstm_128_loss = lstm_128['lossHistory']
lstm_256_loss = lstm_256['lossHistory']
line1, = plt.plot(stepCount*128, lstm_32_loss,color = 'b')
line2, = plt.plot(stepCount*128, lstm_64_loss,color = 'g')
line3, = plt.plot(stepCount*128, lstm_128_loss,color = 'y')
line4, = plt.plot(stepCount*128, lstm_256_loss,color = 'r')
plt.legend([line1, line2, line3, line4], ["Train loss_32 units",  "Train loss_64 units", 
           "Train loss_128 units", "Train loss_256 units"], loc = 'upper right')
plt.xlabel('Number of iterations')
plt.ylabel('Train loss')
plt.title('Training loss against number of iterations')
plt.show()








